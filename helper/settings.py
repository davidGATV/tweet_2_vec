import coloredlogs, logging
import warnings, os
warnings.filterwarnings('ignore')

logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG', logger=logger)

# ================================================================
# API Params
# ================================================================
api_host: str = os.getenv("API_HOST") if "API_HOST" in os.environ else "localhost"
api_port: int = int(os.getenv("API_PORT")) if "API_PORT" in os.environ else 5001
api_base_url: str = os.getenv("API_BASE_URL") if "API_BASE_URL" in os.environ else f"http://{api_host}:{api_port}"
ui_base_path: str = os.getenv("UI_BASE_PATH") if "UI_BASE_PATH" in os.environ else os.path.join(
    "", "www")
static_folder: str = os.path.join(ui_base_path, "static")
templates_folder: str = os.path.join(ui_base_path, "templates")

# ================================================================
# MONGODB Params
# ================================================================

db_host: str = os.getenv("MONGODB_HOST") if "MONGODB_HOST" in os.environ else "localhost"
db_port: str = os.getenv("MONGODB_PORT") if "MONGODB_PORT" in os.environ else "27017"
db_username: str = os.getenv("MONGODB_USERNAME") if "MONGODB_USERNAME" in os.environ else ""
db_password: str = os.getenv("MONGODB_PASSWORD") if "MONGODB_PASSWORD" in os.environ else ""
db_name: str = os.getenv("MONGODB_NAME") if "MONGODB_NAME" in os.environ else "twitter_human_bots"
collection_name: str = os.getenv("MONGODB_COLLECTION_NAME") if "MONGODB_COLLECTION_NAME" in\
                                                               os.environ else "doc_features"

# ================================================================
# Twitter Data
# ================================================================

CONSUMER_KEY = "WcIJMyBF3spFEtRGHwqDlAKDT"
CONSUMER_SECRET = "IfNlsgNrW5o6rfhayOURF1BcUDBcsrfgqss9g5inqeFytDbkgE"
ACCESS_TOKEN = "973174862159253505-mAYpqjzegRXFNhdEPXOkDLsHWXePp7q"
ACCESS_TOKEN_SECRET = "0mmfQcNDJBIFhMM9bexZSO1eIKhFdaP8JX9cMnBG81gJE"

# =========================================================
# Model Directory Parameters
# =========================================================

parent_dir: str = os.getenv("PARENT_DIR") if "PARENT_DIR" in os.environ else ""
model_name: str = os.getenv("BOT_NET_NAME") if "BOT_NET_NAME" in os.environ else\
    "BOT-NET.h5"
history_name: str = os.getenv("HIST_NET_NAME") if "HIST_NET_NAME" in os.environ else\
    model_name + "_history.json"

result_params_dir: str = os.getenv("RES_PARAMS_DIR") if "RES_PARAMS_DIR" in os.environ else\
    "result_params"
model_directory: str = os.path.join(parent_dir, result_params_dir, "trained_models")
history_directory: str = model_directory
scaler_filename: str = os.path.join(parent_dir, result_params_dir, "scaler.pkl")
all_distances: dict = {"0": "angular", "1": "euclidean",
                       "2": "manhattan", "3": "hamming",
                       "4": "dot"}
distance: str = all_distances["3"]

ann_directory: str = os.path.join(parent_dir, result_params_dir,
                                  "ann_model_" + distance + ".ann")

# =========================================================
# Neural Network Parameters
# =========================================================

test_size: float = 0.1
dev_size: float = 0.3
metrics: str = "accuracy"
loss: str = "binary_crossentropy"
optimizer_name: str = "adagrad"
batch_size: int = 128
epochs: int = 300
monitor_early_stopping: str = "val_loss"
patience: int = 15
n_blocks: int = 2
output_dim: int = 1
intermediate_layer: str = "selu"
output_layer: str = "sigmoid"
inverse_neurons: int = 1
conv_model: bool = True
embedding_layer_name: str = "embedding"

# =========================================================
link_information: list = ["",
                          "",
                          ""]
join_attr: str = ". "
default_value: str = "unknown"
output_mapping: dict = {"human": 0, "bot": 1}
popularity_metric: str = "popularity"
id_col: str = "id"
boolean_cols: list = ["default_profile", "default_profile_image",
                      "geo_enabled", "verified"]
drop_num_cols: list = [id_col]
cat_cols: list = ["description", "screen_name", "lang"]

document_col: str = "document"
target_col: str = "account_type"
doc_emb_col: str = "doc_embedding"
uuid_col: str = "uuid"
# ========================================================
http_response_500: str = "Internal Server Error"
http_response_200: str = "Successful Operation"
http_response_400: str = "Bad Request"