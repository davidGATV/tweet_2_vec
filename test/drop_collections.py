from managers.mongodb_manager import MongoDBManager

from helper.settings import (logger, db_host, db_port,
                             db_name, db_username,
                             db_password)


mongo_manager: MongoDBManager = MongoDBManager(
            host=db_host, port=db_port,
            username=db_username, password=db_password,
            db_name=db_name)

mongo_manager.set_up_db()

collection_name:str = "doc_features_flair_rnn"
mongo_manager.remove_all_documents_from_collection(collection_name=collection_name)
