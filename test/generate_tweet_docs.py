import pandas as pd
import os, sys

if "test" in os.getcwd():
    sys.path.append('../')
    parent_dir = ".."
else:
    parent_dir = ""
os.environ["PARENT_DIR"] = parent_dir

from helper.settings import (popularity_metric, boolean_cols, drop_num_cols,
                             scaler_filename, cat_cols, default_value, link_information,
                             document_col, id_col, target_col, join_attr, logger)
from managers.data_manager import DataManager


if __name__ == "__main__":

    dm: DataManager = DataManager()

    resources_dir = os.path.join(parent_dir, "resources")
    dataset_dir = "twitter_account_db_v2.csv"
    dataset_dir = os.path.join(resources_dir, dataset_dir)

    data: pd.DataFrame = dm.load_dataset(
        filename_dir=dataset_dir)

    # 1. Flair embeddings + Pool
    """dm.collection_name = "doc_features_flair_pool"

    dm.generate_features_from_dataframe(
        data=data,
        popularity_metric=popularity_metric,
        boolean_cols=boolean_cols,
        drop_num_cols=drop_num_cols,
        scaler_filename=scaler_filename,
        cat_cols=cat_cols,
        default_value=default_value,
        link_information=link_information,
        document_col=document_col,
        id_col=id_col,
        target_col=target_col,
        join_attr=join_attr,
        update=True, embeddings_names="flair")

    # 2. Bert embedding + pool
    dm.collection_name = "doc_features_bert_pool"

    dm.generate_features_from_dataframe(
        data=data,
        popularity_metric=popularity_metric,
        boolean_cols=boolean_cols,
        drop_num_cols=drop_num_cols,
        scaler_filename=scaler_filename,
        cat_cols=cat_cols,
        default_value=default_value,
        link_information=link_information,
        document_col=document_col,
        id_col=id_col,
        target_col=target_col,
        join_attr=join_attr,
        update=False,
        embeddings_names="bert")

    # 3. Flair + Bert embedding + pool
    dm.collection_name = "doc_features_bert_flair_pool"

    dm.generate_features_from_dataframe(
        data=data,
        popularity_metric=popularity_metric,
        boolean_cols=boolean_cols,
        drop_num_cols=drop_num_cols,
        scaler_filename=scaler_filename,
        cat_cols=cat_cols,
        default_value=default_value,
        link_information=link_information,
        document_col=document_col,
        id_col=id_col,
        target_col=target_col,
        join_attr=join_attr,
        update=False, embeddings_names="all")"""

    # 4. Flair + RNN
    dm.collection_name = "doc_features_flair_rnn"

    dm.generate_features_from_dataframe(
        data=data,
        popularity_metric=popularity_metric,
        boolean_cols=boolean_cols,
        drop_num_cols=drop_num_cols,
        scaler_filename=scaler_filename,
        cat_cols=cat_cols,
        default_value=default_value,
        link_information=link_information,
        document_col=document_col,
        id_col=id_col,
        target_col=target_col,
        join_attr=join_attr,
        update=False,
        embeddings_names="flair",
        doc2vec="rnn")

    # 5. Bert embedding + RNN
    dm.collection_name = "doc_features_bert_rnn"

    dm.generate_features_from_dataframe(
        data=data,
        popularity_metric=popularity_metric,
        boolean_cols=boolean_cols,
        drop_num_cols=drop_num_cols,
        scaler_filename=scaler_filename,
        cat_cols=cat_cols,
        default_value=default_value,
        link_information=link_information,
        document_col=document_col,
        id_col=id_col,
        target_col=target_col,
        join_attr=join_attr,
        update=False,
        embeddings_names="bert",
        doc2vec="rnn")

    # 6. Flair + Bert embedding + rnn
    dm.collection_name = "doc_features_bert_flair_rnn"

    dm.generate_features_from_dataframe(
        data=data,
        popularity_metric=popularity_metric,
        boolean_cols=boolean_cols,
        drop_num_cols=drop_num_cols,
        scaler_filename=scaler_filename,
        cat_cols=cat_cols,
        default_value=default_value,
        link_information=link_information,
        document_col=document_col,
        id_col=id_col,
        target_col=target_col,
        join_attr=join_attr,
        update=False,
        embeddings_names="all", doc2vec="rnn")

    # 7. Transformer
    dm.collection_name = "doc_features_bert_transformer"

    dm.generate_features_from_dataframe(
        data=data,
        popularity_metric=popularity_metric,
        boolean_cols=boolean_cols,
        drop_num_cols=drop_num_cols,
        scaler_filename=scaler_filename,
        cat_cols=cat_cols,
        default_value=default_value,
        link_information=link_information,
        document_col=document_col,
        id_col=id_col,
        target_col=target_col,
        join_attr=join_attr,
        update=False, embeddings_names="bert",
        doc2vec="transformer")