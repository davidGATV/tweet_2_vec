import os, pickle
import numpy as np
from tensorflow.keras.models import Model
from sklearn.manifold import TSNE
import pandas as pd
import seaborn as sns
import sys
sys.path.append("..")
from helper.settings import (logger, model_name, model_directory,
                             collection_name, target_col,
                             doc_emb_col, output_dim)
from managers.mongodb_manager import MongoDBManager
from managers.data_manager import DataManager
from managers.model_manager import ModelManager

sns.set_style("whitegrid")


if __name__ == "__main__":
    dm: DataManager = DataManager()
    mongo_manager: MongoDBManager = dm.mongo_manager
    input_dim: int = dm.get_input_dim_from_doc()
    perplexity: int = 80
    figure_embedding_name: str = os.path.join("",
                                              "figures", "embeddings_" +
                                              str(model_name) + "_ " +
                                              str(perplexity) + ".png")

    # 1. Get all ids
    logger.info("Retrieving all the documents identifiers")
    all_docs_ids: iter = dm.retrieve_all_documents_ids()
    all_docs_ids: list = [i for i in all_docs_ids]
    x: np.ndarray = np.empty((len(all_docs_ids), input_dim))
    y: list = []

    logger.info("Extracting the embeddings from MongoDB")
    # 2. For each id, retrieve doc_embedding + label and concatenate into numpy
    for i, uuid in enumerate(all_docs_ids):
        doc_data: dict = mongo_manager.get_document_by_id(
            collection_name=collection_name,
            uuid=uuid)
        x_np: np.ndarray = pickle.loads(doc_data.get(doc_emb_col, None))
        x[i, ] = x_np
        y.append(doc_data.get(target_col, ""))

    # 3. Load Model
    logger.info("Loading the trained model")
    model_commit: str = "4a86fa230df5478ea44359cd158155ef"
    model_directory: str = os.path.join("..", "mlruns", model_commit, "artifacts", "model", "data")
    model_name: str = "model.h5"
    model_manager: ModelManager = ModelManager(input_dim=input_dim, output_dim=output_dim)
    model = model_manager.load_trained_model(
        model_directory=model_directory,
        model_name=model_name)
    model: Model = model_manager.compile_model(model=model)

    # 4. Predict the low-dimensional vector using the network
    intermediate_layer_model: Model = Model(inputs=model.input,
                                            outputs=model.layers[-2].output)
    x_output: np.ndarray = intermediate_layer_model.predict(x)

    # 5. Apply T-SNE
    logger.info("Computing the T-SNE Algorithm")
    tsne: TSNE = TSNE(perplexity=perplexity, learning_rate=2000, early_exaggeration=2)
    tsne_embeddings: np.ndarray = tsne.fit_transform(x_output)
    print(tsne_embeddings.shape)

    data: dict = {"tsne-1": tsne_embeddings[:, 0].tolist(),
                  "tsne-2": tsne_embeddings[:, 1].tolist(),
                  "account_type": y}

    df_tsne: pd.DataFrame = pd.DataFrame(data)
    df_tsne.to_csv("df_tsne.csv")

    # 6. Plot result
    ax = sns.scatterplot(x="tsne-1", y="tsne-2", hue="account_type",
                         data=df_tsne, s=50)
    fig = ax.get_figure()
    fig.savefig(figure_embedding_name)

