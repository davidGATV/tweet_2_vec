from analysis.feature_extraction import FeatureExtraction


document: str = "Me encanta la música. No puedo dejar de escucharla yuhu"
embeddings: list = FeatureExtraction.get_flair_embeddings()
print(embeddings)

doc_embedding = FeatureExtraction.generate_doc_embedding(
    document=document,
    embeddings=embeddings,
    doc2vec="pool")

print(doc_embedding.shape)


doc_embedding_flair_bert = FeatureExtraction.generate_doc_embedding(
    document=document,
    embeddings=embeddings+FeatureExtraction.get_bert_embeddings(),
    doc2vec="pool")

print(doc_embedding_flair_bert.shape)

doc_embedding_bert = FeatureExtraction.generate_doc_embedding(
    document=document,
    embeddings=FeatureExtraction.get_bert_embeddings(),
    doc2vec="pool")

print(doc_embedding_bert.shape)


doc_embedding_bert_rnn = FeatureExtraction.generate_doc_embedding(
    document=document,
    embeddings=FeatureExtraction.get_bert_embeddings(),
    doc2vec="rnn")

print(doc_embedding_bert_rnn.shape)

# =================================================================
# Transformer
doc_embedding_trans = FeatureExtraction.generate_doc_embedding(
    document=document,
    embeddings=embeddings,
    doc2vec="transformer")

print(doc_embedding_trans.shape)

