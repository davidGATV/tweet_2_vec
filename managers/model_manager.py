import os
import numpy as np
import tensorflow as tf
from helper.settings import (logger, test_size, dev_size,
                             model_name, metrics,
                             loss, optimizer_name, batch_size, epochs,
                             monitor_early_stopping, patience, output_mapping,
                             target_col)
from sklearn.model_selection import train_test_split
from sklearn.utils.class_weight import compute_class_weight
from helper.utils import (prepare_directory, write_json_file, read_json_file)
from typing import Optional
from helper.sequence_generator import DataGenerator
from managers.mongodb_manager import MongoDBManager
from tensorflow.keras import optimizers
from tensorflow.keras.models import Model
from tensorflow.keras import initializers
from tensorflow.keras.layers import (Input, Dense, BatchNormalization,
                                     Conv1D, MaxPooling1D, Flatten, Dropout)
from tensorflow.keras.callbacks import History, EarlyStopping
from tensorflow.keras.metrics import (Precision, Recall, TrueNegatives,
                                      TruePositives, FalseNegatives,
                                      FalsePositives)


class ModelManager:
    def __init__(self, input_dim: int, output_dim: int):
        self.test_size = test_size
        self.dev_size = dev_size
        self.input_dim: int = input_dim
        self.output_dim: int = output_dim
        self.model_name: str = model_name
        self.custom_metrics: dict = self.get_custom_metrics()
        self.metrics: list = [metrics] + [i for i in list(self.custom_metrics.values())]
        self.losses: str = loss
        self.optimizer_name: str = optimizer_name
        self.batch_size: int = batch_size
        self.epochs: int = epochs
        self.callbacks: list = [EarlyStopping(monitor=monitor_early_stopping,
                                              patience=patience,
                                              restore_best_weights=True)]

    @staticmethod
    def get_custom_metrics():
        custom_metrics: dict = {"precision": Precision(),
                                "recall": Recall(),
                                "true_positives": TruePositives(),
                                "true_negatives": TrueNegatives(),
                                "false_negatives": FalseNegatives(),
                                "false_positives": FalsePositives()}
        return custom_metrics

    def generate_train_dev_test_sets(self, all_docs_ids: list):
        train_ids: list = []
        dev_ids: list = []
        test_ids: list = []
        try:
            ys: list = [output_mapping[i[target_col]] for i in all_docs_ids]

            # Divide document ids into train and test
            train_ids, test_ids = train_test_split(all_docs_ids,
                                                   test_size=self.test_size,
                                                   stratify=ys, shuffle=True)
            ys_tr: list = [output_mapping[i[target_col]] for i in train_ids]

            train_ids, dev_ids = train_test_split(train_ids,
                                                  test_size=self.dev_size,
                                                  stratify=ys_tr, shuffle=True)
        except Exception as e:
            logger.error(e)
        return train_ids, dev_ids, test_ids

    @staticmethod
    def get_kernel_initializer(activation_function: str):
        kernel_initializer: initializers = initializers.glorot_normal()
        try:
            if activation_function == "relu" or activation_function == "leaky_relu":
                kernel_initializer: initializers = initializers.he_normal()
            elif activation_function == "selu" or activation_function == "elu":
                kernel_initializer: initializers = initializers.lecun_normal()
            else:
                pass
        except Exception as e:
            logger.error(e)
        return kernel_initializer

    @staticmethod
    def build_densenet_201(input_dim: int, output_dim: int):
        model: Optional[Model] = None

        try:
            input_data: Input = Input((input_dim,))

            model: Model = tf.keras.applications.DenseNet201(
                include_top=True,
                weights="imagenet",
                input_tensor=input_data,
                input_shape=None,
                pooling=None,
                classes=output_dim + 1,
            )
        except Exception as e:
            logger.error(e)
        return model

    @staticmethod
    def build_baseline_dnn_model(input_dim: int, output_dim: int, intermediate_neurons: int,
                                 intermediate_layer: str, model_name: str, output_layer: str,
                                 embedding_layer_name: str, n_blocks: int = 2):
        model: Optional[Model] = None
        try:
            # Create inputs
            input_data: Input = Input((input_dim,))
            kernel_initializer: initializers = __class__.get_kernel_initializer(
                activation_function=intermediate_layer)

            # Input Block
            x: BatchNormalization = BatchNormalization()(input_data)
            x: Dense = Dense(intermediate_neurons, activation=intermediate_layer,
                             kernel_initializer=kernel_initializer)(x)

            # Hidden Blocks
            for i in range(n_blocks):
                x: BatchNormalization = BatchNormalization()(x)
                # Add Dense Layer
                hidden_neurons: int = int(np.ceil(intermediate_neurons / 2**(i+1)))
                if i < n_blocks-1:
                    x: Dense = Dense(hidden_neurons,
                                     activation=intermediate_layer,
                                     kernel_initializer=kernel_initializer)(x)
                else:
                    x: Dense = Dense(hidden_neurons,
                                     activation=intermediate_layer,
                                     name=embedding_layer_name,
                                     kernel_initializer=kernel_initializer)(x)

            # Output Block
            kernel_initializer_output: initializers = __class__.get_kernel_initializer(
                activation_function=output_layer)

            output_data: Dense = Dense(output_dim,
                                       activation=output_layer,
                                       kernel_initializer=kernel_initializer_output)(x)

            model: Model = Model(
                inputs=input_data,
                outputs=output_data,
                name=model_name)
        except Exception as e:
            logger.error(e)
        return model

    @staticmethod
    def build_conv_baseline(input_dim: int, output_dim: int, intermediate_neurons: int,
                            intermediate_layer: str, model_name: str, output_layer: str,
                            embedding_layer_name: str, n_blocks: int = 2,
                            filters: int = 256):
        model: Optional[Model] = None
        try:
            input_data_shape: tuple = (input_dim, 1)
            # Create inputs
            input_data: Input = Input(input_data_shape)

            x = Conv1D(filters=filters, kernel_size=1, activation=intermediate_layer,
                       input_shape=input_data_shape)(input_data)
            x: BatchNormalization = BatchNormalization()(x)
            filters: int = int(np.ceil(filters/2))
            # Hidden blocks
            for i in range(n_blocks):
                x = Conv1D(filters=filters, kernel_size=1, activation=intermediate_layer)(x)
                x = MaxPooling1D(pool_size=1)(x)
                x = Dropout(0.4)(x)

            # Output Blocks
            x = Flatten(name=embedding_layer_name)(x)
            x = Dense(512, activation=intermediate_layer)(x)
            x = Dropout(0.4)(x)
            x = Dense(256, activation=intermediate_layer)(x)
            output_data = Dense(output_dim, activation=output_layer)(x)

            model: Model = Model(
                inputs=input_data,
                outputs=output_data,
                name=model_name)

        except Exception as e:
            logger.error(e)
        return model

    @staticmethod
    def build_bot_net(input_dim: int,
                      output_dim: int,
                      intermediate_neurons: int,
                      intermediate_layer: str,
                      model_name: str, output_layer: str,
                      embedding_layer_name: str,
                      n_blocks: int = 2, conv_model: bool = False):
        bot_net: Optional[Model] = None
        try:
            if not conv_model:
                # Dense-based model
                bot_net: Model = __class__.build_baseline_dnn_model(
                    input_dim=input_dim,
                    output_dim=output_dim,
                    intermediate_neurons=intermediate_neurons,
                    intermediate_layer=intermediate_layer,
                    output_layer=output_layer,
                    model_name=model_name,
                    embedding_layer_name=embedding_layer_name,
                    n_blocks=n_blocks)
            else:
                # Conv-based model
                bot_net: Model = __class__.build_conv_baseline(
                    input_dim=input_dim,
                    output_dim=output_dim,
                    intermediate_neurons=intermediate_neurons,
                    intermediate_layer=intermediate_layer,
                    output_layer=output_layer,
                    model_name=model_name,
                    embedding_layer_name=embedding_layer_name,
                    n_blocks=n_blocks)

            # Show summary
            bot_net.summary()
        except Exception as e:
            logger.error(e)
        return bot_net

    @staticmethod
    def select_optimizer(opt_name="adam"):
        optimizer: Optional[optimizers] = None
        try:
            if opt_name == 'sgd':
                optimizer = optimizers.SGD()
            elif opt_name == 'adagrad':
                optimizer = optimizers.Adagrad()
            elif opt_name == 'adadelta':
                optimizer = optimizers.Adadelta()
            elif opt_name == 'adam':
                optimizer = optimizers.Adam()
            elif opt_name == 'nadam':
                optimizer = optimizers.Nadam()
            else:
                logger.warning('Not valid optimizer!. Please check the configuration script.')
        except Exception as e:
            logger.error(e)
        return optimizer

    def compile_model(self, model: Model):
        try:
            optimizer: optimizers = self.select_optimizer(opt_name=self.optimizer_name)
            model.compile(
                optimizer=optimizer,
                loss=self.losses,
                metrics=self.metrics)
        except Exception as e:
            logger.error(e)
        return model

    def train_model(self, model: Model, mongo_manager: MongoDBManager,
                    collection_name: str, train_ids: list, dev_ids: list,
                    conv_model: bool, class_weights: dict):
        history: Optional[History] = None
        try:
            steps_per_epoch = int(len(train_ids) / self.batch_size)
            validation_steps = int(len(dev_ids) / self.batch_size)

            add_axis: bool = True if conv_model else False
            print("Add axis", add_axis)
            training_generator: DataGenerator = DataGenerator(
                mongo_manager=mongo_manager,
                docs_uuids=train_ids,
                collection_name=collection_name,
                batch_size=self.batch_size,
                dim_embd=self.input_dim,
                dim_output=self.output_dim,
                shuffle=True, to_fit=True,
                add_axis=add_axis)

            validation_generator: DataGenerator = DataGenerator(
                mongo_manager=mongo_manager,
                docs_uuids=dev_ids,
                collection_name=collection_name,
                batch_size=self.batch_size,
                dim_embd=self.input_dim,
                dim_output=self.output_dim,
                shuffle=True, to_fit=True,
                add_axis=add_axis)

            history: History = model.fit(
                training_generator,
                validation_data=validation_generator,
                class_weight=class_weights,
                steps_per_epoch=steps_per_epoch,
                validation_steps=validation_steps,
                epochs=self.epochs,
                callbacks=self.callbacks)
        except Exception as e:
            logger.error(e)
        return history

    @staticmethod
    def execute_prediction(model: Model, input_test_doc: np.ndarray):
        y_hat: np.ndarray = np.array([])
        try:
            y_hat: np.ndarray = model.predict(input_test_doc)

        except Exception as e:
            logger.error(e)
        return y_hat

    @staticmethod
    def get_model_predictions(model: Model, x_docs: np.ndarray):
        y_hats: np.ndarray = np.array([])
        try:
            # 1. Execute prediction
            y_hats: np.ndarray = __class__.execute_prediction(
                model=model,
                input_test_doc=x_docs)
            y_hats: np.ndarray = y_hats.ravel().reshape((-1, 1))
        except Exception as e:
            logger.error(e)
        return y_hats

    @staticmethod
    def evaluate_model(model: Model, x: np.ndarray, y: np.ndarray, metrics_names: list):

        x: np.ndarray = np.expand_dims(x, axis=2)
        print(x.shape)
        metrics_values: list = model.evaluate(x=x, y=y)
        return dict(zip(metrics_names, metrics_values))

    @staticmethod
    def save_history(history: History, history_directory: str, history_name: str):
        try:
            # Check if directory exists
            prepare_directory(dir_path_to_check=history_directory)
            history_path = os.path.join(history_directory, history_name)
            # Write JSON
            write_json_file(data=history.history, filename=history_path)
        except Exception as e:
            logger.error(e)

    @staticmethod
    def load_history(history_directory: str, history_name: str):
        model_history: Optional[History] = None
        try:
            history_path: str = os.path.join(history_directory, history_name)
            # Load model history
            model_history: History = read_json_file(history_path)
        except Exception as e:
            logger.error(e)
        return model_history

    @staticmethod
    def load_trained_model(model_directory: str, model_name: str):
        model: Optional[Model] = None
        try:
            model_path: str = os.path.join(model_directory, model_name)
            custom_objects: dict = __class__.get_custom_metrics()
            # Load model
            model: Model = tf.keras.models.load_model(model_path, custom_objects, compile=False)
            # load_model(model_path, custom_objects=custom_objects)
        except Exception as e:
            logger.error(e)
        return model

    @staticmethod
    def save_trained_model(model: Model, model_directory: str, model_name: str, extension: str = ".h5"):
        try:
            # Check if directory exists
            prepare_directory(dir_path_to_check=model_directory)
            # Check if extension is not included in the name of the model
            if len(model_name.split(".")) <= 1:
                model_name += extension
            # Merge path
            model_path: str = os.path.join(model_directory, model_name)
            # Save model
            model.save(model_path)
        except Exception as e:
            logger.error(e)

    @staticmethod
    def get_class_weights(train_ids_info: list, target_col: str):
        class_weights: dict = {}
        try:
            y_train: np.ndarray = np.array([i[target_col] for i in train_ids_info])
            class_weights_np: np.ndarray = compute_class_weight(
                'balanced',
                np.unique(y_train.ravel()),
                y_train.ravel())
            class_weights = dict(enumerate(class_weights_np))
        except Exception as e:
            logger.error(e)
        return class_weights