from tweepy.models import User, Status
from dotmap import DotMap
from datetime import datetime
from helper.utils import preprocess_date, get_average_tweets_per_day, get_account_age_in_days
from bson.binary import Binary
from langdetect import detect


class TwitterDocument:
    default: str = "unknown"

    def dict_from_class(self):
        return dict((key, value)
                    for (key, value) in self.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)

    @staticmethod
    def get_language(text: str):
        lang: str = __class__.default
        try:
            lang: str = detect(text=text)
        except Exception as e:
            pass
        return lang


class UserProfile(TwitterDocument):
    def __init__(self, user_profile: User):
        self.created_at: datetime = preprocess_date(user_profile.__getattribute__(
            "created_at"))
        self.default_profile: bool = user_profile.__getattribute__(
            "default_profile")
        self.default_profile_image: bool = user_profile.__getattribute__(
            "default_profile_image")
        self.description: str = user_profile.__getattribute__(
            "description")
        self.favourites_count: int = user_profile.__getattribute__(
            "favourites_count")
        self.followers_count: int = user_profile.__getattribute__(
            "followers_count")
        self.friends_count: int = user_profile.__getattribute__(
            "friends_count")
        self.geo_enabled: bool = user_profile.__getattribute__(
            "geo_enabled")
        self.id: int = user_profile.__getattribute__("id")
        self.lang: str = self.get_language(text=user_profile.__getattribute__(
            "lang")) if user_profile.__getattribute__("lang") is not None else \
            self.get_language(text=user_profile.__getattribute__("description"))
        self.location: str = self.default if user_profile.__getattribute__(
            "location") is None or user_profile.__getattribute__(
            "location") == "" else user_profile.__getattribute__("location")
        self.profile_background_image_url: str = user_profile.__getattribute__(
            "profile_background_image_url")
        self.profile_image_url: str = user_profile.__getattribute__(
            "profile_image_url")
        self.screen_name: str = user_profile.__getattribute__(
            "screen_name")
        self.statuses_count: int = user_profile.__getattribute__(
            "statuses_count")
        self.verified: bool = user_profile.__getattribute__(
            "verified")
        self.average_tweets_per_day: float = get_average_tweets_per_day(
            statuses_count=self.statuses_count, created_at=user_profile.__getattribute__(
                "created_at"))
        self.account_age_days: int = get_account_age_in_days(
            created_at=user_profile.__getattribute__("created_at"))


class InputFeatureBotDocument(TwitterDocument):
    def __init__(self, uuid: str,
                 account_type: str, doc_embedding: Binary):
        self.uuid: str = uuid
        self.account_type: str = account_type
        self.doc_embedding: Binary = doc_embedding


class UserAccount(UserProfile):
    def __init__(self, user_profile: User, bot_prob: float = -1):
        super().__init__(user_profile=user_profile)
        self.name: str = user_profile.__getattribute__("name")
        self.url: str = user_profile.__getattribute__("url")
        try:
            self.profile_banner_url: str = user_profile.__getattribute__("profile_banner_url")
        except Exception as e:
            self.profile_banner_url: str = \
                "https://i.picsum.photos/id/1021/2048/1206.jpg?hmac=fqT2NWHx783Pily1V_39ug_GFH1A4GlbmOMu8NWB3Ts"
        self.profile_url: str = "https://twitter.com/" + self.screen_name.replace("@", "")
        self.listed_count: str = user_profile.__getattribute__("listed_count")
        self.bot_prob: float = bot_prob


class TweetInfo(TwitterDocument):
    def __init__(self, status: Status):
        self.created_at: datetime = preprocess_date(status.__getattribute__(
            "created_at"))
        self.id: int = status.__getattribute__("id")
        self.entities: dict = status.__getattribute__("entities")
        self.source: str = status.__getattribute__("source")
        self.is_quote_status: bool = status.__getattribute__("is_quote_status")
        self.text: str = status.__getattribute__("text")
        self.retweet_count: int = status.__getattribute__("retweet_count")
        self.retweeted: bool = status.__getattribute__("retweeted")
        self.user: dict = status.__getattribute__("user")
        self.possibly_sensitive: bool = status.__getattribute__("possibly_sensitive") if\
            hasattr(status, 'possibly_sensitive') else False
        self.favorite_count: int = status.__getattribute__("favorite_count")
        self.favorited: bool = status.__getattribute__("favorited")
        self.lang: str = detect(status.__getattribute__("text"))


class TweetStatus(TweetInfo):
    def __init__(self, status: Status):
        super().__init__(status=status)
        self.url: str = f"https://twitter.com/user/status/{status.__getattribute__('id')}"
