from models.twitter_models import TwitterDocument


class ServiceAPIDoc(TwitterDocument):
    def __init__(self, status: int, message: str, output: dict):
        self.status: int = status
        self.message: str = message
        self.output: dict = output
